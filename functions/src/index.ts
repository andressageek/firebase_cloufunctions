// import * as admin from 'firebase-admin';
// admin.initializeApp(functions.config().firebase);
import * as functions from 'firebase-functions';
import * as express from 'express';

const app = express();
// Habilita a descoberta do IP do cliente
app.set('trust proxy', true);

// API PUBLICA
const nomes = require('./apis/public/Nomes/nomes_v1');
app.use('/v1', nomes);

const app_secure = express();
// Habilita a descoberta do IP do cliente
app.set('trust proxy', true);

export const api_publica = functions.https.onRequest(app);
