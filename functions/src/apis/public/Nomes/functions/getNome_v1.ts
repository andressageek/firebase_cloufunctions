import {MessageStatus} from "~/core/core.helper";

export const getNome_v1 = (): MessageStatus => {
  try {
      const nomes = [
          'Andressa',
          'Andrade'
      ];
      const random = Math.floor(Math.random() * 2);
      return {
          message: 'OK',
          status: true,
          metadata: {
              nome: nomes[random]
          }
      };
  }
  catch (erro) {
      return {
          message: 'Erro',
          erro: erro.message,
          status: false
      };
  }
};
