import * as express from 'express';
import {configuraMiddleware} from "~/apis/utils/configuraMiddleware";

import {getNome_v1} from "./functions/getNome_v1";

const router = configuraMiddleware(express.Router());
// Adiciona body-parser para fazer parser de 'application/json', 'application/x-www-form-urlencoded'
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

/**
 *
 */
router.get('/nome', async (req: express.Request, res: express.Response) => {

    try {
        const nome = getNome_v1();
        if (nome.status) {
            return res.status(200).json(nome);
        }
        else {
            return res.status(500).json(nome);
        }
    }
    catch(erro) {
        return res.status(500).json({
            message: 'Erro',
            status: false,
            erro: erro.message
        });
    }

});

module.exports = router;
