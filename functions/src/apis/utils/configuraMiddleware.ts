import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';
import * as cors from 'cors';
import * as helmet from 'helmet';

export const configuraMiddleware = (expressAPI) => {

    // Configuração de cors
    const corsOptions: cors.CorsOptions = {

        /**
         *
         * Checa a origem do cabeçalho de CORS
         *
         * @param origin Origem presente no cabeçalho
         * @param callback Callback a ser executado após a execução da função
         */
        origin: (origin, callback) => {
            callback(null, true);
        }
    };
    // Configuração para limitação de número máximo de requisições
    const reqLimits = {
            /**
             * Define a janela de tempo em ms para cada evento de limite
             */
            windowMs: 600000,

            /**
             * Limita cada IP a úm certo número de requisições
             */
            max: 50
        };
    // Configura o CORS para todos os paths
    expressAPI.use(cors(corsOptions));
    // Configura a ferramenta para limite de requisiçoes
    expressAPI.use(rateLimit(reqLimits));
    // Adiciona suporte a compressão zlib para diminuir e agilizar o tempo de transferência
    expressAPI.use(compression());
    // Adiciona algumas funções de middleware para segurança
    expressAPI.use(helmet());

    return expressAPI;

};
