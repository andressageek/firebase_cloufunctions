/**
 * Modelo de retorno de mensagens de funções auxiliares
 */
export class MessageStatus {

    /**
     * Mensagem de retorno da operação
     */
    message: string;

    /**
     * Status da operação
     */
    status: boolean;

    /**
     * Dados extras
     */
    metadata?: any;

    /**
     * Retorna detalhes técnicos do problema [opcional]
     */
    erro?: string | Array<string>;

    /**
     * Construtor de objetos de retorno
     *
     * @param message
     * @param metadata
     * @param erro
     */
    constructor( message: string, metadata?: object, erro?: string) {
        if(erro) {
            this.metadata = metadata ? metadata : {};
            this.status = false;
            this.erro = erro;
            this.message = message;
        }
        else {
            this.metadata = metadata ? metadata : {};
            this.status = true;
            this.message = message;
        }
    }
}
