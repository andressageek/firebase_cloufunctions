"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageStatus = void 0;
/**
 * Modelo de retorno de mensagens de funções auxiliares
 */
class MessageStatus {
    /**
     * Construtor de objetos de retorno
     *
     * @param message
     * @param metadata
     * @param erro
     */
    constructor(message, metadata, erro) {
        if (erro) {
            this.metadata = metadata ? metadata : {};
            this.status = false;
            this.erro = erro;
            this.message = message;
        }
        else {
            this.metadata = metadata ? metadata : {};
            this.status = true;
            this.message = message;
        }
    }
}
exports.MessageStatus = MessageStatus;
//# sourceMappingURL=core.helper.js.map