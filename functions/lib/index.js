"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.api_publica = void 0;
// import * as admin from 'firebase-admin';
// admin.initializeApp(functions.config().firebase);
const functions = require("firebase-functions");
const express = require("express");
const app = express();
// Habilita a descoberta do IP do cliente
app.set('trust proxy', true);
// API PUBLICA
const nomes = require('./apis/public/Nomes/nomes_v1');
app.use('/v1', nomes);
exports.api_publica = functions.https.onRequest(app);
//# sourceMappingURL=index.js.map