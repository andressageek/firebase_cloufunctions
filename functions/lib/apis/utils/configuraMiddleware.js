"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configuraMiddleware = void 0;
const compression = require("compression");
const rateLimit = require("express-rate-limit");
const cors = require("cors");
const helmet = require("helmet");
exports.configuraMiddleware = (expressAPI) => {
    // Configuração de cors
    const corsOptions = {
        /**
         *
         * Checa a origem do cabeçalho de CORS
         *
         * @param origin Origem presente no cabeçalho
         * @param callback Callback a ser executado após a execução da função
         */
        origin: (origin, callback) => {
            callback(null, true);
        }
    };
    // Configuração para limitação de número máximo de requisições
    const reqLimits = {
        /**
         * Define a janela de tempo em ms para cada evento de limite
         */
        windowMs: 600000,
        /**
         * Limita cada IP a úm certo número de requisições
         */
        max: 50
    };
    // Configura o CORS para todos os paths
    expressAPI.use(cors(corsOptions));
    // Configura a ferramenta para limite de requisiçoes
    expressAPI.use(rateLimit(reqLimits));
    // Adiciona suporte a compressão zlib para diminuir e agilizar o tempo de transferência
    expressAPI.use(compression());
    // Adiciona algumas funções de middleware para segurança
    expressAPI.use(helmet());
    return expressAPI;
};
//# sourceMappingURL=configuraMiddleware.js.map