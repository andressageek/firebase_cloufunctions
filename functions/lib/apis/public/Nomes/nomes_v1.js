"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const configuraMiddleware_1 = require("../../utils/configuraMiddleware");
const getNome_v1_1 = require("./functions/getNome_v1");
const router = configuraMiddleware_1.configuraMiddleware(express.Router());
// Adiciona body-parser para fazer parser de 'application/json', 'application/x-www-form-urlencoded'
router.use(express.json());
router.use(express.urlencoded({ extended: true }));
/**
 *
 */
router.get('/nome', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const nome = getNome_v1_1.getNome_v1();
        if (nome.status) {
            return res.status(200).json(nome);
        }
        else {
            return res.status(500).json(nome);
        }
    }
    catch (erro) {
        return res.status(500).json({
            message: 'Erro',
            status: false,
            erro: erro.message
        });
    }
}));
module.exports = router;
//# sourceMappingURL=nomes_v1.js.map