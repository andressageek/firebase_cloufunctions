"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getNome = void 0;
exports.getNome = () => {
    try {
        const nomes = [
            'Andressa',
            'Andrade'
        ];
        const random = Math.floor(Math.random() * 2);
        return {
            message: 'OK',
            status: true,
            metadata: {
                nome: nomes[random]
            }
        };
    }
    catch (erro) {
        return {
            message: 'Erro',
            erro: erro.message,
            status: false
        };
    }
};
//# sourceMappingURL=getNome.js.map