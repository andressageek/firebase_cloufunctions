"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express = require("express");
const configuraMiddleware_1 = require("../../utils/configuraMiddleware");
const getNome_1 = require("./getNome");
exports.router = express.Router();
exports.router = configuraMiddleware_1.configuraMiddleware(exports.router);
// Adiciona body-parser para fazer parser de 'application/json', 'application/x-www-form-urlencoded'
exports.router.use(express.json());
exports.router.use(express.urlencoded({ extended: true }));
/**
 *
 */
exports.router.get('/nome', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const nome = getNome_1.getNome();
        if (nome.status) {
            return res.status(200).json(nome);
        }
        else {
            return res.status(500).json(nome);
        }
    }
    catch (erro) {
        return res.status(500).json({
            message: 'Erro',
            status: false,
            erro: erro.message
        });
    }
}));
module.exports = exports.router;
//# sourceMappingURL=nomes.js.map